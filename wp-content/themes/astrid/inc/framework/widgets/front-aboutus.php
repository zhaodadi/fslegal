<?php

/**

 * Services widget

 *

 * @package Astrid

 */



class Atframework_AboutUs extends WP_Widget {



	public function __construct() {

		$widget_ops = array('classname' => 'atframework_aboutus_widget', 'description' => __( 'Show what services you are able to provide.', 'astrid') );

        parent::__construct(false, $name = __('Astrid FP: About Us', 'astrid'), $widget_ops);

		$this->alt_option_name = 'atframework_aboutus_widget';

			

    }

	

	function form($instance) {

		$pageids  		= isset( $instance['pageids'] ) ? esc_html( $instance['pageids'] ) : '';		

	?>

	

	<p><label for="<?php echo $this->get_field_id( 'pageids' ); ?>"><?php _e( 'Page IDs to display in this widget (separated by commas, example: 14,810,220). Note: you can find the page ID in the URL bar while editing your page.', 'astrid' ); ?></label>

	<input id="<?php echo $this->get_field_id( 'pageids' ); ?>" name="<?php echo $this->get_field_name( 'pageids' ); ?>" type="text" value="<?php echo $pageids; ?>" size="3" /></p>

	

	<?php

	}



	function update($new_instance, $old_instance) {

		$instance = $old_instance;	

		$instance['pageids'] 		= sanitize_text_field($new_instance['pageids']);		

		    			

		$alloptions = wp_cache_get( 'alloptions', 'options' );

		if ( isset($alloptions['atframework_aboutus']) )

			delete_option('atframework_aboutus');		  

		  

		return $instance;

	}

	

	function widget($args, $instance) {

		$cache = array();

		if ( ! $this->is_preview() ) {

			$cache = wp_cache_get( 'atframework_aboutus', 'widget' );

		}



		if ( ! is_array( $cache ) ) {

			$cache = array();

		}



		if ( ! isset( $args['widget_id'] ) ) {

			$args['widget_id'] = $this->id;

		}



		if ( isset( $cache[ $args['widget_id'] ] ) ) {

			echo $cache[ $args['widget_id'] ];

			return;

		}



		ob_start();

		extract($args);



		$pageids		= isset( $instance['pageids'] ) ? esc_html($instance['pageids']) : '';

      	$widget_title_color = isset($instance['widget_title_color']) ? $instance['widget_title_color'] : null; 



		$ids = $pageids;



		$services = new WP_Query( array(

			'post_type'			=> 'page',

			'no_found_rows' 	=> true,

			'post_status'   	=> 'publish',

			'orderby' 			=> 'menu_order',

			'order'   			=> 'ASC',

			'p' 				=> $ids			

		) );



		echo $args['before_widget']; 



		if ($services->have_posts()) :

?>

		<?php while ( $services->have_posts() ) : $services->the_post(); ?>
			<h2 class="widget-title" style="color: <?php echo $widget_title_color ?>">
				<span class="title-decoration"><?php the_title(); ?></span>
			</h2>
			<div class="textwidget">
				<?php the_content(); ?>
			</div>
		<?php endwhile; ?>
	<?php

		wp_reset_postdata();

		endif;

		echo $args['after_widget'];

		if ( ! $this->is_preview() ) {

			$cache[ $args['widget_id'] ] = ob_get_flush();

			wp_cache_set( 'atframework_about2', $cache, 'widget' );

		} else {

			ob_end_flush();

		}

	}

	

}