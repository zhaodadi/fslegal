
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `wp_yoast_seo_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_yoast_seo_meta` (
  `object_id` bigint(20) unsigned NOT NULL,
  `internal_link_count` int(10) unsigned DEFAULT NULL,
  `incoming_link_count` int(10) unsigned DEFAULT NULL,
  UNIQUE KEY `object_id` (`object_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `wp_yoast_seo_meta` WRITE;
/*!40000 ALTER TABLE `wp_yoast_seo_meta` DISABLE KEYS */;
INSERT INTO `wp_yoast_seo_meta` VALUES (1,0,0),(6,0,0),(86,7,0),(88,0,2),(92,0,0),(94,0,0),(98,0,0),(100,0,0),(102,0,0),(106,0,0),(108,0,0),(110,0,0),(112,0,1),(114,0,1),(116,0,1),(118,0,0),(120,0,0),(122,0,0),(124,0,0),(226,5,1),(228,0,0),(230,0,0),(236,0,0),(250,0,0),(252,0,0),(254,0,0),(290,0,0),(291,0,0),(295,0,0),(305,6,0),(306,0,0),(317,0,1),(320,0,1),(328,0,0),(329,0,0),(331,0,1),(334,0,1),(336,0,1),(341,0,1),(343,0,1),(346,0,1),(348,0,1),(350,0,1),(352,0,1),(354,0,1),(366,0,0),(397,0,0),(401,0,0),(403,0,0),(406,0,0),(409,0,0),(412,0,0),(419,0,0),(420,0,0),(421,0,0),(422,0,0),(423,0,0),(424,0,0),(425,0,0),(426,0,0),(428,0,0),(429,0,0),(430,0,0),(431,0,0),(434,0,0),(439,0,0),(447,0,0),(448,0,0),(459,0,0),(462,0,0),(463,0,0),(464,0,0),(474,0,0),(475,0,0),(481,0,0),(482,0,0),(483,0,0),(500,0,0),(502,0,0),(503,0,0),(505,0,0),(507,0,0),(513,0,0),(514,0,0),(520,0,0),(526,0,0),(536,0,0),(537,0,0),(540,0,0),(541,0,0),(545,0,0),(546,0,0),(548,0,0),(550,0,0),(553,0,0),(554,0,0),(556,0,0),(563,0,0),(576,0,0),(582,0,0),(583,0,0),(586,0,0),(592,0,0),(593,0,0),(631,0,0),(632,0,0),(633,0,0),(634,0,0),(637,0,0),(644,0,0),(645,0,0),(646,0,0),(649,0,0),(652,0,0),(653,0,0),(663,0,0),(670,0,0),(671,0,0),(680,0,0),(684,0,0),(685,0,0),(887,0,0),(888,0,0),(891,0,0),(897,0,0),(898,0,0),(899,0,0),(900,0,0),(901,0,0),(902,0,0),(905,0,0),(906,0,0),(907,0,0),(910,0,0),(911,0,0),(912,0,0),(913,0,0),(914,0,0),(915,0,0),(916,0,0),(917,0,0),(918,0,0),(919,0,0),(920,0,0),(921,0,0),(922,0,0),(965,0,0),(966,0,0),(967,0,0),(968,0,0),(969,0,0),(972,0,0),(1171,0,0),(1176,0,0),(1177,0,0),(1178,0,0),(1179,0,0),(1180,0,0),(1203,0,0),(1204,0,0),(1212,0,0),(1214,0,0),(1216,0,0),(1218,0,0),(1219,0,0),(1220,0,0),(1408,0,0),(1497,0,0),(1612,0,0),(1666,0,0),(1667,0,0),(1692,0,0),(1693,0,0),(1697,0,0),(1698,0,0),(1770,0,0);
/*!40000 ALTER TABLE `wp_yoast_seo_meta` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

