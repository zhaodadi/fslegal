
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `wp_yoast_seo_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_yoast_seo_links` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_id` bigint(20) unsigned NOT NULL,
  `target_post_id` bigint(20) unsigned NOT NULL,
  `type` varchar(8) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `link_direction` (`post_id`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=908 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `wp_yoast_seo_links` WRITE;
/*!40000 ALTER TABLE `wp_yoast_seo_links` DISABLE KEYS */;
INSERT INTO `wp_yoast_seo_links` VALUES (248,'https://www.acacia-au.com/partner-migration.php',409,0,'external'),(249,'https://www.acacia-au.com/parent.php',409,0,'external'),(250,'https://www.acacia-au.com/visitor-visas-for-parents.php',409,0,'external'),(251,'https://www.acacia-au.com/remaining-relative.php',409,0,'external'),(252,'https://www.acacia-au.com/carer-visas.php',409,0,'external'),(253,'https://www.acacia-au.com/retirement-visa-australia.php',409,0,'external'),(257,'mailto:visa@fslgeal.com.au',403,0,'external'),(323,'https://www.mara.gov.au/media/95420/Code_of_Conduct.pdf',94,0,'external'),(706,'http://www.fslegal.com.au/services/personal-injury/',305,226,'internal'),(707,'http://www.fslegal.com.au/services/criminal-law/',305,116,'internal'),(708,'http://www.fslegal.com.au/services/property-law/',305,88,'internal'),(709,'http://www.fslegal.com.au/services/family-law/',305,112,'internal'),(710,'http://www.fslegal.com.au/services/migration-law/',305,114,'internal'),(711,'http://www.fslegal.com.au/services/property-law/',305,88,'internal'),(767,'mailto:office@fslegal.com.au',483,0,'external'),(833,'http://www.fslegal.com.au/services/commercial-law/commercial-contracts/',86,341,'internal'),(834,'http://www.fslegal.com.au/services/commercial-law/sale-purchase-of-business/',86,343,'internal'),(835,'http://www.fslegal.com.au/services/commercial-law/franchise/',86,346,'internal'),(836,'http://www.fslegal.com.au/services/commercial-law/business-law/',86,348,'internal'),(837,'http://www.fslegal.com.au/services/commercial-law/intellectual-property/',86,350,'internal'),(838,'http://www.fslegal.com.au/services/commercial-law/debt-recovery/',86,352,'internal'),(839,'http://www.fslegal.com.au/services/commercial-law/employment-law/',86,354,'internal'),(885,'http://www.fslegal.com.au/services/personal-injury/car-accident/',226,317,'internal'),(886,'http://www.fslegal.com.au/services/personal-injury/medical-negligence/',226,320,'internal'),(887,'http://www.fslegal.com.au/services/personal-injury/slip-and-fall/',226,331,'internal'),(888,'http://www.fslegal.com.au/services/personal-injury/work-related-injuries/',226,334,'internal'),(889,'http://www.fslegal.com.au/services/personal-injury/defamation/',226,336,'internal'),(897,'https://www.police.nsw.gov.au/crime/domestic_and_family_violence/apprehended_violence_orders_avo',230,0,'external'),(901,'https://www.mara.gov.au/becoming-an-agent/professional-standards-and-obligations/code-of-conduct/',505,0,'external'),(907,'https://www.mara.gov.au/becoming-an-agent/professional-standards-and-obligations/code-of-conduct/',114,0,'external');
/*!40000 ALTER TABLE `wp_yoast_seo_links` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

